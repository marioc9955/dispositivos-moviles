1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.animateproperty"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="12"
8-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml
9        android:targetSdkVersion="23" />
9-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml
10
11    <application
11-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:6:5-25:19
12        android:debuggable="true"
13        android:icon="@drawable/ic_launcher"
13-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:7:9-45
14        android:theme="@style/AppTheme" >
14-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:8:9-40
15        <activity
15-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:9:9-16:20
16            android:name="com.examples.animateproperty.FlipperActivity"
16-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:10:13-44
17            android:label="Flipper" >
17-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:11:13-36
18            <intent-filter>
18-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:12:13-15:29
19                <action android:name="android.intent.action.MAIN" />
19-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:13:17-68
19-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:13:25-66
20
21                <category android:name="android.intent.category.LAUNCHER" />
21-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:14:17-76
21-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:14:27-74
22            </intent-filter>
23        </activity>
24        <activity
24-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:17:9-24:20
25            android:name="com.examples.animateproperty.FlipperPauseActivity"
25-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:18:13-49
26            android:label="FlipperPause" >
26-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:19:13-41
27            <intent-filter>
27-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:12:13-15:29
28                <action android:name="android.intent.action.MAIN" />
28-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:13:17-68
28-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:13:25-66
29
30                <category android:name="android.intent.category.LAUNCHER" />
30-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:14:17-76
30-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter_01_Source_Code\1_04_AnimateProperty\src\main\AndroidManifest.xml:14:27-74
31            </intent-filter>
32        </activity>
33    </application>
34
35</manifest>
