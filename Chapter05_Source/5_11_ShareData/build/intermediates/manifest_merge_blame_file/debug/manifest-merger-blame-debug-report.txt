1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.examples.share"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
7-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:7:5-9:40
8        android:minSdkVersion="4"
8-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:8:9-34
9        android:targetSdkVersion="24" />
9-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:9:9-38
10
11    <application
11-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:11:5-25:19
12        android:debuggable="true"
13        android:icon="@drawable/icon"
13-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:12:9-38
14        android:label="@string/app_name"
14-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:13:9-41
15        android:testOnly="true"
16        android:theme="@style/AppTheme" >
16-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:14:9-40
17        <activity android:name="com.androidrecipes.share.ShareActivity" >
17-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:15:9-20:20
17-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:15:19-48
18            <intent-filter>
18-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:16:13-19:29
19                <action android:name="android.intent.action.MAIN" />
19-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:17:17-68
19-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:17:25-66
20
21                <category android:name="android.intent.category.LAUNCHER" />
21-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:18:17-76
21-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:18:27-74
22            </intent-filter>
23        </activity>
24
25        <provider
25-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:21:9-24:20
26            android:name="com.androidrecipes.share.ImageProvider"
26-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:22:13-42
27            android:authorities="com.androidrecipes.share.imageprovider" >
27-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter05_Source\5_11_ShareData\src\main\AndroidManifest.xml:23:13-73
28        </provider>
29    </application>
30
31</manifest>
