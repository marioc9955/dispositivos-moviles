package uce.edu.ec.fing.pedidos.modelo;


import android.content.ContentValues;
import android.database.Cursor;

import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.Productos;

public class Producto {

    public String idProducto;

    public String nombre;

    public float precio;

    public int existencias;

    public Producto(String idProducto, String nombre, float precio, int existencias) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
        this.existencias = existencias;
    }

    public Producto(String nombre, float precio, int existencias) {
        this.idProducto = Productos.generarIdProducto();
        this.nombre = nombre;
        this.precio = precio;
        this.existencias = existencias;
    }

    public Producto(Cursor cursor){
        idProducto = cursor.getString(cursor.getColumnIndex(Productos.ID));
        nombre = cursor.getString(cursor.getColumnIndex(Productos.NOMBRE));
        existencias = cursor.getInt(cursor.getColumnIndex(Productos.EXISTENCIAS));
        precio = cursor.getFloat(cursor.getColumnIndex(Productos.PRECIO));
    }

    public String getIdProducto() {
        return idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public int getExistencias() {
        return existencias;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Productos.ID, idProducto);
        values.put(Productos.NOMBRE, nombre);
        values.put(Productos.EXISTENCIAS, existencias);
        values.put(Productos.PRECIO, precio);
        return values;
    }

    @Override
    public String toString() {
        return nombre+" Precio: "+precio+" stock: "+existencias;
    }
}
