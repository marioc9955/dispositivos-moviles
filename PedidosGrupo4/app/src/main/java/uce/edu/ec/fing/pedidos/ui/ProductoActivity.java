package uce.edu.ec.fing.pedidos.ui;

import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.MenuItem;

import java.util.Calendar;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.pedidos.modelo.FormaPago;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.BaseDatosPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;

public class ProductoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_PRODUCTO_ID = "extra_producto_id";
    public static final String EXTRA_DETALLE_ID = "extra_detalle_id";
    public static final String EXTRA_CABECERA_ID = "extra_cabecera_id";
    public static final String EXTRA_CLIENTE_ID = "extra_cliente_id";
    public static final String EXTRA_MDP_ID = "extra_mdp_id";

    public static String tablaActual = "producto";

    OperacionesBaseDatos datos;

    public class TareaPruebaDatos extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            // [INSERCIONES]
            String fechaActual = Calendar.getInstance().getTime().toString();

            try {

                datos.getDb().beginTransaction();

                // Inserción Clientes
                String cliente1 = datos.insertarCliente(new Cliente(null, "Veronica", "Del Topo", "4552000"));
                String cliente2 = datos.insertarCliente(new Cliente(null, "Carlos", "Villagran", "4440000"));

                // Inserción Formas de pago
                String formaPago1 = datos.insertarFormaPago(new FormaPago(null, "Efectivo"));
                String formaPago2 = datos.insertarFormaPago(new FormaPago(null, "Crédito"));

                // Inserción Productos
                String producto1 = datos.insertarProducto(new Producto(null, "Manzana unidad", 2, 100));
                String producto2 = datos.insertarProducto(new Producto(null, "Pera unidad", 3, 230));
                String producto3 = datos.insertarProducto(new Producto(null, "Guayaba unidad", 5, 55));
                String producto4 = datos.insertarProducto(new Producto(null, "Maní unidad", 3.6f, 60));

                // Inserción Pedidos
                String pedido1 = datos.insertarCabeceraPedido(
                        new CabeceraPedido(null, fechaActual, cliente1, formaPago1));
                String pedido2 = datos.insertarCabeceraPedido(
                        new CabeceraPedido(null, fechaActual,cliente2, formaPago2));

                // Inserción Detalles
                datos.insertarDetallePedido(new DetallePedido(pedido1, 1, producto1, 5, 2));
                datos.insertarDetallePedido(new DetallePedido(pedido1, 2, producto2, 10, 3));
                datos.insertarDetallePedido(new DetallePedido(pedido2, 1, producto3, 30, 5));
                datos.insertarDetallePedido(new DetallePedido(pedido2, 2, producto4, 20, 3.6f));

                // Eliminación Pedido
                //datos.eliminarCabeceraPedido(pedido1);

                // Actualización Cliente
                datos.actualizarCliente(new Cliente(cliente2, "Carlos Alberto", "Villagran", "3333333"));

                datos.getDb().setTransactionSuccessful();
            } finally {
                datos.getDb().endTransaction();
            }

            // [QUERIES]
            Log.d("Clientes","Clientes");
            DatabaseUtils.dumpCursor(datos.obtenerClientes());
            Log.d("Formas de pago", "Formas de pago");
            DatabaseUtils.dumpCursor(datos.obtenerFormasPago());
            Log.d("Productos", "Productos");
            DatabaseUtils.dumpCursor(datos.obtenerProductos());
            Log.d("Cabeceras de pedido", "Cabeceras de pedido");
            DatabaseUtils.dumpCursor(datos.obtenerCabecerasPedidos());
            Log.d("Detalles de pedido", "Detalles de pedido");
            DatabaseUtils.dumpCursor(datos.obtenerDetallesPedido());

            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav_bar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CambiarTabla(tablaActual);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getApplicationContext().deleteDatabase(BaseDatosPedidos.NOMBRE_BASE_DATOS);
        
        datos = OperacionesBaseDatos
                .obtenerInstancia(getApplicationContext());

        new TareaPruebaDatos().execute();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_producto:
                CambiarTabla("producto");
                break;
            case R.id.nav_cliente:
                CambiarTabla("cliente");
                break;
            case R.id.nav_cabecera:
                CambiarTabla("cabecera");
                break;
            case R.id.nav_detalle:
                CambiarTabla("detalle");
                break;
            case R.id.nav_metodoDP:
                CambiarTabla("mdp");
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void CambiarTabla(String tabla){
        switch (tabla){
            case "producto":
                tablaActual = "producto";
                setTitle("Productos");
                Fragment prodFrag = ProductoFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.producto_container, prodFrag)
                        .commit();
                break;
            case "cliente":
                tablaActual = "cliente";
                setTitle("Clientes");
                Fragment clienteFrag = ClienteFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.producto_container, clienteFrag)
                        .commit();
                break;
            case "cabecera":
                tablaActual = "cabecera";
                setTitle("Pedidos");
                Fragment cabFrag = CabeceraFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.producto_container, cabFrag)
                        .commit();
                break;
            case "detalle":
                tablaActual = "detalle";
                setTitle("Detalles");
                Fragment detalleFrag = DetallePedidoFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.producto_container, detalleFrag)
                        .commit();
                break;
            case "mdp":
                tablaActual = "mdp";
                setTitle("Metodos de pago");
                Fragment mdpFrag = MetodoDePagoFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.producto_container, mdpFrag)
                        .commit();
                break;
        }

    }
}