package uce.edu.ec.fing.pedidos.modelo;

import android.content.ContentValues;
import android.database.Cursor;

import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;

public class DetallePedido {

    public String idCabeceraPedido;

    public int secuencia;

    public String idProducto;

    public int cantidad;

    public float precio;

    public DetallePedido(String idCabeceraPedido, int secuencia,
                         String idProducto, int cantidad, float precio) {
        this.idCabeceraPedido = idCabeceraPedido;
        this.secuencia = secuencia;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public DetallePedido(int secuencia,
                         String idProducto, int cantidad, float precio) {
        this.secuencia = secuencia;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public DetallePedido(Cursor cursor){
        idCabeceraPedido = cursor.getString(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.ID));
        secuencia = cursor.getInt(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.SECUENCIA));
        idProducto = cursor.getString(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.ID_PRODUCTO));
        cantidad = cursor.getInt(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.CANTIDAD));
        precio = cursor.getFloat(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.PRECIO));
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ContratoPedidos.DetallesPedido.ID, idCabeceraPedido);
        values.put(ContratoPedidos.DetallesPedido.SECUENCIA, secuencia);
        values.put(ContratoPedidos.DetallesPedido.CANTIDAD, cantidad);
        values.put(ContratoPedidos.DetallesPedido.PRECIO, precio);
        values.put(ContratoPedidos.DetallesPedido.ID_PRODUCTO, idProducto);
        return values;
    }
}
