package uce.edu.ec.fing.pedidos.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.List;

import uce.edu.ec.fing.pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.pedidos.modelo.FormaPago;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.BaseDatosPedidos.Tablas;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.CabecerasPedido;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.Clientes;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.DetallesPedido;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.FormasPago;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos.Productos;

/**
 * Clase auxiliar que implementa a {@link uce.edu.ec.fing.pedidos.sqlite.BaseDatosPedidos} para llevar a cabo el CRUD
 * sobre las entidades existentes.
 */

public final class OperacionesBaseDatos {

    private static BaseDatosPedidos baseDatos;

    private static OperacionesBaseDatos instancia = new OperacionesBaseDatos();


    private OperacionesBaseDatos() {
    }

    public static OperacionesBaseDatos obtenerInstancia(Context contexto) {
        if (baseDatos == null) {
            baseDatos = new BaseDatosPedidos(contexto);
        }
        return instancia;
    }

    //<editor-fold desc="OPERACIONES_CABECERA_PEDIDO">
    // [OPERACIONES_CABECERA_PEDIDO]
    public Cursor obtenerCabecerasPedidos() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

//        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
//
//        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);
//
//        return builder.query(db, proyCabeceraPedido, null, null, null, null, null);

        return db.rawQuery("SELECT * FROM "+Tablas.CABECERA_PEDIDO, null);
    }

    public List<CabeceraPedido> obtenerListaCabeceras(){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        Cursor cabecerasCursor = db.rawQuery("Select * From " + Tablas.CABECERA_PEDIDO, null);

        List<CabeceraPedido> clientesLista = new ArrayList<>();

        CabeceraPedido cp;
        cabecerasCursor.moveToFirst();

        while (!cabecerasCursor.isAfterLast()) {
            String id = cabecerasCursor.getString(cabecerasCursor.getColumnIndex(CabecerasPedido.ID));
            String fecha = cabecerasCursor.getString(cabecerasCursor.getColumnIndex(CabecerasPedido.FECHA));
            String idCliente = cabecerasCursor.getString(cabecerasCursor.getColumnIndex(CabecerasPedido.ID_CLIENTE));
            String idMDP = cabecerasCursor.getString(cabecerasCursor.getColumnIndex(CabecerasPedido.ID_FORMA_PAGO));
            cp = new CabeceraPedido(id, fecha, idCliente, idMDP);
            clientesLista.add(cp);
            cabecerasCursor.moveToNext();
        }

        return clientesLista;
    }

    public Cursor obtenerCabeceraCursorPorId(String id) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

//        String selection = String.format("%s=?", CabecerasPedido.ID);
//        String[] selectionArgs = {id};
//
//        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
//        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);
//
//        String[] proyeccion = {
//                Tablas.CABECERA_PEDIDO + "." + CabecerasPedido.ID,
//                CabecerasPedido.FECHA,
//                Clientes.NOMBRES,
//                Clientes.APELLIDOS,
//                FormasPago.NOMBRE};
//
//        return builder.query(db, proyeccion, selection, selectionArgs, null, null, null);
        Cursor c = db.query(
                Tablas.CABECERA_PEDIDO,
                null,
                CabecerasPedido.ID + " LIKE ?",
                new String[]{id},
                null,
                null,
                null);
        return c;
    }

    public long insertCabecera(CabeceraPedido pedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idCabeceraPedido = CabecerasPedido.generarIdCabeceraPedido();


        ContentValues valores = new ContentValues();
        valores.put(CabecerasPedido.ID, idCabeceraPedido);
        valores.put(CabecerasPedido.FECHA, pedido.fecha);
        valores.put(CabecerasPedido.ID_CLIENTE, pedido.idCliente);
        valores.put(CabecerasPedido.ID_FORMA_PAGO, pedido.idFormaPago);

        return db.insert(Tablas.CABECERA_PEDIDO, null, valores);

    }

    public String insertarCabeceraPedido(CabeceraPedido pedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idCabeceraPedido = CabecerasPedido.generarIdCabeceraPedido();

        ContentValues valores = new ContentValues();
        valores.put(CabecerasPedido.ID, idCabeceraPedido);
        valores.put(CabecerasPedido.FECHA, pedido.fecha);
        valores.put(CabecerasPedido.ID_CLIENTE, pedido.idCliente);
        valores.put(CabecerasPedido.ID_FORMA_PAGO, pedido.idFormaPago);

        // Insertar cabecera
        db.insertOrThrow(Tablas.CABECERA_PEDIDO, null, valores);

        return idCabeceraPedido;
    }

    public int updateCabecera(CabeceraPedido cp, String cabId){
        return baseDatos.getWritableDatabase().update(Tablas.CABECERA_PEDIDO,
                cp.toContentValues(),
                CabecerasPedido.ID + " LIKE ?",
                new String[]{cabId});
    }

    public int eliminarCabeceraPedido(String idCabeceraPedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = CabecerasPedido.ID + "=?";
        String[] whereArgs = {idCabeceraPedido};

        return db.delete(Tablas.CABECERA_PEDIDO, whereClause, whereArgs);
    }

    public Cursor getCabeceraById(String cabId){
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor c = db.query(
                Tablas.CABECERA_PEDIDO,
                null,
                CabecerasPedido.ID + " LIKE ?",
                new String[]{cabId},
                null,
                null,
                null);
        return c;
    }
    // [/OPERACIONES_CABECERA_PEDIDO]
    //</editor-fold>

    //<editor-fold desc="OPERACIONES_DETALLE_PEDIDO">
    // [OPERACIONES_DETALLE_PEDIDO]
    public Cursor obtenerDetallesPedido() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.DETALLE_PEDIDO);

        return db.rawQuery(sql, null);
    }

    public List<DetallePedido> obtenerListaDetallesPorIdCabecera(String idCabecera){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

//        Cursor detallesCursor = db.rawQuery("Select * From " + Tablas.DETALLE_PEDIDO
//                +" Where "+DetallesPedido.ID+"=?", new String[]{idCabecera});

        Cursor detallesCursor = db.query(
                Tablas.DETALLE_PEDIDO,
                null,
                DetallesPedido.ID + " LIKE ?",
                new String[]{idCabecera},
                null,
                null,
                DetallesPedido.SECUENCIA);

        List<DetallePedido> detallesLista = new ArrayList<>();

        DetallePedido dp;
        detallesCursor.moveToFirst();

        while (!detallesCursor.isAfterLast()) {
            int secuencia = detallesCursor.getInt(detallesCursor.getColumnIndex(DetallesPedido.SECUENCIA));
            int cantidad = detallesCursor.getInt(detallesCursor.getColumnIndex(DetallesPedido.CANTIDAD));
            float precio = detallesCursor.getFloat(detallesCursor.getColumnIndex(DetallesPedido.PRECIO));
            String idProducto = detallesCursor.getString(detallesCursor.getColumnIndex(DetallesPedido.ID_PRODUCTO));
            dp = new DetallePedido(idCabecera, secuencia, idProducto, cantidad, precio);
            detallesLista.add(dp);
            detallesCursor.moveToNext();
        }

        return detallesLista;
    }

    public Cursor obtenerDetallesPorIdPedidoYSec(String idCabeceraPedido, int secuencia) {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=? AND %s="+secuencia,
                Tablas.DETALLE_PEDIDO, CabecerasPedido.ID, DetallesPedido.SECUENCIA);

        String[] selectionArgs = {idCabeceraPedido};

        Cursor c =db.rawQuery(sql, selectionArgs);

        return c;

    }

    public int updateDetalle(DetallePedido dp, String detId, int sec){
        return baseDatos.getWritableDatabase().update(Tablas.DETALLE_PEDIDO,
                dp.toContentValues(),
                DetallesPedido.ID + " LIKE ? AND " + DetallesPedido.SECUENCIA + " LIKE "+sec,
                new String[]{detId});
    }

    public long insertDetallePedido(DetallePedido detalle) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(DetallesPedido.ID, detalle.idCabeceraPedido);
        valores.put(DetallesPedido.SECUENCIA, detalle.secuencia);
        valores.put(DetallesPedido.ID_PRODUCTO, detalle.idProducto);
        valores.put(DetallesPedido.CANTIDAD, detalle.cantidad);
        valores.put(DetallesPedido.PRECIO, detalle.precio);

        return db.insert(Tablas.DETALLE_PEDIDO, null, valores);

    }

    public String insertarDetallePedido(DetallePedido detalle) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(DetallesPedido.ID, detalle.idCabeceraPedido);
        valores.put(DetallesPedido.SECUENCIA, detalle.secuencia);
        valores.put(DetallesPedido.ID_PRODUCTO, detalle.idProducto);
        valores.put(DetallesPedido.CANTIDAD, detalle.cantidad);
        valores.put(DetallesPedido.PRECIO, detalle.precio);

        db.insertOrThrow(Tablas.DETALLE_PEDIDO, null, valores);

        return String.format("%s#%d", detalle.idCabeceraPedido, detalle.secuencia);

    }

    public boolean actualizarDetallePedido(DetallePedido detalle) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(DetallesPedido.SECUENCIA, detalle.secuencia);
        valores.put(DetallesPedido.CANTIDAD, detalle.cantidad);
        valores.put(DetallesPedido.PRECIO, detalle.precio);

        String selection = String.format("%s=? AND %s=?",
                DetallesPedido.ID, DetallesPedido.SECUENCIA);
        final String[] whereArgs = {detalle.idCabeceraPedido, String.valueOf(detalle.secuencia)};

        int resultado = db.update(Tablas.DETALLE_PEDIDO, valores, selection, whereArgs);

        return resultado > 0;
    }

    public int eliminarDetallePedido(String idCabeceraPedido, int secuencia) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String selection = String.format("%s=? AND %s=?",
                DetallesPedido.ID, DetallesPedido.SECUENCIA);
        String[] whereArgs = {idCabeceraPedido, String.valueOf(secuencia)};

        int resultado = db.delete(Tablas.DETALLE_PEDIDO, selection, whereArgs);

        return resultado;
    }
    // [/OPERACIONES_DETALLE_PEDIDO]
    //</editor-fold>

    //<editor-fold desc="OPERACIONES_PRODUCTO">
    // [OPERACIONES_PRODUCTO]

    public Producto obtenerProductoID(String id) {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                Tablas.PRODUCTO, Productos.ID);

        String[] selectionArgs = {id};

        Cursor cursor = db.rawQuery(sql, selectionArgs);
        Producto producto = null;
        cursor.moveToFirst(); //apunta el primer registro de la tabla

        while (!cursor.isAfterLast()) { //permite recorrer todos los registros
            String nombre = cursor.getString(cursor.getColumnIndex(Productos.NOMBRE));
            float precio=cursor.getFloat(cursor.getColumnIndex(Productos.PRECIO));
            int existencias=cursor.getInt(cursor.getColumnIndex(Productos.EXISTENCIAS));
            producto = new Producto(id,nombre,precio,existencias);
            cursor.moveToNext(); //mueve al siguiente registro
        }

        return producto;

    }

    public Cursor getProductoById(String productoId){
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor c = db.query(
                Tablas.PRODUCTO,
                null,
                Productos.ID + " LIKE ?",
                new String[]{productoId},
                null,
                null,
                null);
        return c;
    }

    public List<Producto> obtenerListaProductos(){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        Cursor productosCursor = db.rawQuery("Select * From " + Tablas.PRODUCTO, null);

        List<Producto> productosLista = new ArrayList<>();

        Producto p;
        productosCursor.moveToFirst();

        while (!productosCursor.isAfterLast()) {
            String id = productosCursor.getString(productosCursor.getColumnIndex(Productos.ID));
            String nombres = productosCursor.getString(productosCursor.getColumnIndex(Productos.NOMBRE));
            float precio = productosCursor.getFloat(productosCursor.getColumnIndex(Productos.PRECIO));
            int existencias = productosCursor.getInt(productosCursor.getColumnIndex(Productos.EXISTENCIAS));
            p = new Producto(id, nombres, precio, existencias);
            productosLista.add(p);
            productosCursor.moveToNext();
        }
        return productosLista;
    }

    public Cursor obtenerProductos() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.PRODUCTO);

        return db.rawQuery(sql, null);
    }

    public long insertProducto(Producto producto) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        // Generar Pk
        String idProducto = Productos.generarIdProducto();
        valores.put(Productos.ID, idProducto);
        valores.put(Productos.NOMBRE, producto.nombre);
        valores.put(Productos.PRECIO, producto.precio);
        valores.put(Productos.EXISTENCIAS, producto.existencias);

        return db.insert(Tablas.PRODUCTO, null, valores);

    }

    public String insertarProducto(Producto producto) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        // Generar Pk
        String idProducto = Productos.generarIdProducto();
        valores.put(Productos.ID, idProducto);
        valores.put(Productos.NOMBRE, producto.nombre);
        valores.put(Productos.PRECIO, producto.precio);
        valores.put(Productos.EXISTENCIAS, producto.existencias);

        db.insertOrThrow(Tablas.PRODUCTO, null, valores);

        return idProducto;

    }

    public int updateProducto(Producto producto, String productoId){
        return baseDatos.getWritableDatabase().update(Tablas.PRODUCTO,
                producto.toContentValues(),
                Productos.ID + " LIKE ?",
                new String[]{productoId});
    }

    public int eliminarProducto(String idProducto) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", Productos.ID);
        String[] whereArgs = {idProducto};

        int resultado = db.delete(Tablas.PRODUCTO, whereClause, whereArgs);

        return resultado;
    }
    // [/OPERACIONES_PRODUCTO]
    //</editor-fold>

    //<editor-fold desc="OPERACIONES_CLIENTE">
    // [OPERACIONES_CLIENTE]

    public List<Cliente> obtenerListaClientes(){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        Cursor clientesCursor = db.rawQuery("Select * From " + Tablas.CLIENTE, null);

        List<Cliente> clientesLista = new ArrayList<>();

        Cliente c;
        clientesCursor.moveToFirst();

        while (!clientesCursor.isAfterLast()) {
            String id = clientesCursor.getString(clientesCursor.getColumnIndex(Clientes.ID));
            String nombres = clientesCursor.getString(clientesCursor.getColumnIndex(Clientes.NOMBRES));
            String apellidos = clientesCursor.getString(clientesCursor.getColumnIndex(Clientes.APELLIDOS));
            String telefono = clientesCursor.getString(clientesCursor.getColumnIndex(Clientes.TELEFONO));
            c = new Cliente(id, nombres, apellidos, telefono);
            clientesLista.add(c);
            clientesCursor.moveToNext();
        }

        return clientesLista;
    }

    public Cliente obtenerClientePorId(String id) {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String[] selectionArgs = {id};

        Cursor cursor = db.rawQuery("SELECT * FROM " + Tablas.CLIENTE + " WHERE "
                + Clientes.ID +"=?", selectionArgs);

        Cliente c = null;
        cursor.moveToFirst(); //apunta el primer registro de la tabla

        while (!cursor.isAfterLast()) { //permite recorrer todos los registros
            String nombres = cursor.getString(cursor.getColumnIndex(Clientes.NOMBRES));
            String apellidos = cursor.getString(cursor.getColumnIndex(Clientes.APELLIDOS));
            String telefono = cursor.getString(cursor.getColumnIndex(Clientes.TELEFONO));
            c = new Cliente(id, nombres, apellidos, telefono);
            cursor.moveToNext(); //mueve al siguiente registro
        }

        return c;

    }

    public Cursor getClienteById(String clienteId){
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor c = db.query(
                Tablas.CLIENTE,
                null,
                Clientes.ID + " LIKE ?",
                new String[]{clienteId},
                null,
                null,
                null);
        return c;
    }
    public long insertCliente(Cliente cliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        // Generar Pk
        String idCliente = Clientes.generarIdCliente();
        valores.put(Clientes.ID, idCliente);
        valores.put(Clientes.NOMBRES, cliente.nombres);
        valores.put(Clientes.APELLIDOS, cliente.apellidos);
        valores.put(Clientes.TELEFONO, cliente.telefono);

        return db.insert(Tablas.CLIENTE, null, valores);

    }
    public int updateCliente(Cliente cliente, String clienteId){
        return baseDatos.getWritableDatabase().update(Tablas.CLIENTE,
                cliente.toContentValues(),
                Clientes.ID + " LIKE ?",
                new String[]{clienteId});
    }

    public Cursor obtenerClientes() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.CLIENTE);

        return db.rawQuery(sql, null);
    }

    public String insertarCliente(Cliente cliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idCliente = Clientes.generarIdCliente();

        ContentValues valores = new ContentValues();
        valores.put(Clientes.ID, idCliente);
        valores.put(Clientes.NOMBRES, cliente.nombres);
        valores.put(Clientes.APELLIDOS, cliente.apellidos);
        valores.put(Clientes.TELEFONO, cliente.telefono);

        return db.insertOrThrow(Tablas.CLIENTE, null, valores) > 0 ? idCliente : null;
    }

    public boolean actualizarCliente(Cliente cliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Clientes.NOMBRES, cliente.nombres);
        valores.put(Clientes.APELLIDOS, cliente.apellidos);
        valores.put(Clientes.TELEFONO, cliente.telefono);

        String whereClause = String.format("%s=?", Clientes.ID);
        final String[] whereArgs = {cliente.idCliente};

        int resultado = db.update(Tablas.CLIENTE, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public int eliminarCliente(String idCliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", Clientes.ID);
        final String[] whereArgs = {idCliente};

        int resultado = db.delete(Tablas.CLIENTE, whereClause, whereArgs);

        return resultado;
    }
    // [/OPERACIONES_CLIENTE]
    //</editor-fold>

    //<editor-fold desc="OPERACIONES_FORMA_PAGO">
    // [OPERACIONES_FORMA_PAGO]

    public List<FormaPago> obtenerListaFormasPago(){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        Cursor mdpCursor = db.rawQuery("Select * From " + Tablas.FORMA_PAGO, null);

        List<FormaPago> mdpLista = new ArrayList<>();

        FormaPago mdp;
        mdpCursor.moveToFirst();

        while (!mdpCursor.isAfterLast()) {
            String id = mdpCursor.getString(mdpCursor.getColumnIndex(FormasPago.ID));
            String nombre = mdpCursor.getString(mdpCursor.getColumnIndex(FormasPago.NOMBRE));
            mdp = new FormaPago(id, nombre);
            mdpLista.add(mdp);
            mdpCursor.moveToNext();
        }

        return mdpLista;
    }

    public FormaPago obtenerFormaPagoID(String mdpId){
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String[] selectionArgs = {mdpId};

        Cursor cursor = db.rawQuery("SELECT * FROM "+ Tablas.FORMA_PAGO +" WHERE "+ FormasPago.ID
                +"=?", selectionArgs);

        FormaPago mdp = null;
        cursor.moveToFirst(); //apunta el primer registro de la tabla

        while (!cursor.isAfterLast()) {
            String id = cursor.getString(cursor.getColumnIndex(FormasPago.ID));
            String nombre = cursor.getString(cursor.getColumnIndex(FormasPago.NOMBRE));
            mdp = new FormaPago(id,nombre);
            cursor.moveToNext();
        }

        return mdp;
    }

    public Cursor getMDPById(String mdpId){
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor c = db.query(
                Tablas.FORMA_PAGO,
                null,
                Productos.ID + " LIKE ?",
                new String[]{mdpId},
                null,
                null,
                null);
        return c;
    }

    public Cursor obtenerFormasPago() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.FORMA_PAGO);

        return db.rawQuery(sql, null);
    }

    public String insertarFormaPago(FormaPago formaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idFormaPago = FormasPago.generarIdFormaPago();

        ContentValues valores = new ContentValues();
        valores.put(FormasPago.ID, idFormaPago);
        valores.put(FormasPago.NOMBRE, formaPago.nombre);

        return db.insertOrThrow(Tablas.FORMA_PAGO, null, valores) > 0 ? idFormaPago : null;
    }

    public long insertFormaPago(FormaPago formaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        // Generar Pk
        String idFormaPago = FormasPago.generarIdFormaPago();
        valores.put(FormasPago.ID, idFormaPago);
        valores.put(FormasPago.NOMBRE, formaPago.nombre);

        return db.insert(Tablas.FORMA_PAGO, null, valores);

    }

    public int actualizarFormaPago(FormaPago formaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(FormasPago.NOMBRE, formaPago.nombre);

        String whereClause = String.format("%s=?", FormasPago.ID);
        String[] whereArgs = {formaPago.idFormaPago};

        int resultado = db.update(Tablas.FORMA_PAGO, valores, whereClause, whereArgs);

        return resultado;
    }

    public int eliminarFormaPago(String idFormaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", FormasPago.ID);
        String[] whereArgs = {idFormaPago};

        int resultado = db.delete(Tablas.FORMA_PAGO, whereClause, whereArgs);

        return resultado;
    }

    public SQLiteDatabase getDb() {
        return baseDatos.getWritableDatabase();
    }


    // [/OPERACIONES_FORMA_PAGO]
    //</editor-fold>


    private static final String CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO = "cabecera_pedido " +
            "INNER JOIN cliente " +
            "ON cabecera_pedido.id_cliente = cliente.id " +
            "INNER JOIN forma_pago " +
            "ON cabecera_pedido.id_forma_pago = forma_pago.id";


    private final String[] proyCabeceraPedido = new String[]{
            Tablas.CABECERA_PEDIDO + "." + CabecerasPedido.ID,
            CabecerasPedido.FECHA,
            Clientes.NOMBRES,
            Clientes.APELLIDOS,
            FormasPago.NOMBRE};

}
