package uce.edu.ec.fing.pedidos.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.FormaPago;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.MetodoDePagoFragment;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;
import uce.edu.ec.fing.pedidos.ui.ProductoFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MetodoDePagoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MetodoDePagoDetailFragment extends Fragment {

    private static final String ARG_MDP_ID = "mdpId";

    private String mMDPId;

    private CollapsingToolbarLayout mCollapsingView;

    private OperacionesBaseDatos mDbHelper;

    public MetodoDePagoDetailFragment() {
        // Required empty public constructor
    }

    public static MetodoDePagoDetailFragment newInstance(String mdpId) {
        MetodoDePagoDetailFragment fragment = new MetodoDePagoDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MDP_ID, mdpId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMDPId = getArguments().getString(ARG_MDP_ID);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_metodo_de_pago_detail, container, false);

        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        loadFormasPago();
        return root;
    }

    private void loadFormasPago() {
        new GetFormaPagoByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                new DeleteFormaPagoTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProductoFragment.REQUEST_UPDATE_DELETE_PRODUCTO) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showMDP(FormaPago formaPago) {
        mCollapsingView.setTitle(formaPago.nombre);
    }

    private void showEditScreen() {
        ProductoActivity.tablaActual = "mdp";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_MDP_ID, mMDPId);
        startActivityForResult(intent, MetodoDePagoFragment.REQUEST_UPDATE_DELETE_MDP);
    }

    private void showMDPScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar forma de pago", Toast.LENGTH_SHORT).show();
    }
    private class GetFormaPagoByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getMDPById(mMDPId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showMDP(new FormaPago(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteFormaPagoTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mDbHelper.eliminarFormaPago(mMDPId);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showMDPScreen(integer > 0);
        }

    }
}