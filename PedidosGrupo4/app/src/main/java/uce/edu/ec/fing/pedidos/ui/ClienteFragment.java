package uce.edu.ec.fing.pedidos.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.ClienteCursorAdapter;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.detail.DetailActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClienteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClienteFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_CLIENTE = 2;

    private ListView mClientesList;
    private ClienteCursorAdapter mClientesAdapter;
    private FloatingActionButton mAddButton;

    private OperacionesBaseDatos mDbHelper;

    public ClienteFragment() {
        // Required empty public constructor
    }

    public static ClienteFragment newInstance() {
        ClienteFragment fragment = new ClienteFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cliente, container, false);
        // Referencias UI
        mClientesList = (ListView) root.findViewById(R.id.clientes_list);
        mClientesAdapter = new ClienteCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mClientesList.setAdapter(mClientesAdapter);

        // Eventos
        mClientesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mClientesAdapter.getItem(i);
                String currentClienteId = currentItem.getString(
                        currentItem.getColumnIndex(ContratoPedidos.Clientes.ID));

                showDetailScreen(currentClienteId);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });

        // Instancia de helper
        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        loadClientes();
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditActivity.REQUEST_ADD:
                    showSuccessfullSavedMessage();
                    loadClientes();
                    break;
                case REQUEST_UPDATE_DELETE_CLIENTE:
                    loadClientes();
                    break;
            }
        }
    }

    private void loadClientes() {
        new ClienteLoadTask().execute();
    }

    private void showSuccessfullSavedMessage() {
        Toast.makeText(getActivity(),
                "Cliente guardado correctamente", Toast.LENGTH_SHORT).show();
    }

    private void showAddScreen() {
        ProductoActivity.tablaActual = "cliente";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD);
    }

    private void showDetailScreen(String lawyerId) {
        ProductoActivity.tablaActual = "cliente";
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_CLIENTE_ID, lawyerId);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_CLIENTE);
    }

    private class ClienteLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerClientes();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mClientesAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }
}