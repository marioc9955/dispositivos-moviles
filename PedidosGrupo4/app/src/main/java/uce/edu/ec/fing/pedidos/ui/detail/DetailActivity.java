package uce.edu.ec.fing.pedidos.ui.detail;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String id;
        switch (ProductoActivity.tablaActual){
            case "producto":
                id = getIntent().getStringExtra(ProductoActivity.EXTRA_PRODUCTO_ID);
                ProductoDetailFragment pdfragment = (ProductoDetailFragment)
                        getSupportFragmentManager().findFragmentById(R.id.producto_detail_container);
                if (pdfragment == null) {
                    pdfragment = ProductoDetailFragment.newInstance(id);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.producto_detail_container, pdfragment)
                            .commit();
                }
                break;
            case "cliente":
                id = getIntent().getStringExtra(ProductoActivity.EXTRA_CLIENTE_ID);
                ClienteDetailFragment cdfragment = (ClienteDetailFragment)
                        getSupportFragmentManager().findFragmentById(R.id.producto_detail_container);
                if (cdfragment == null) {
                    cdfragment = ClienteDetailFragment.newInstance(id);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.producto_detail_container, cdfragment)
                            .commit();
                }
                break;
            case "mdp":
                id = getIntent().getStringExtra(ProductoActivity.EXTRA_MDP_ID);
                MetodoDePagoDetailFragment mdpdfragment = (MetodoDePagoDetailFragment)
                        getSupportFragmentManager().findFragmentById(R.id.producto_detail_container);
                if (mdpdfragment == null) {
                    mdpdfragment = MetodoDePagoDetailFragment.newInstance(id);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.producto_detail_container, mdpdfragment)
                            .commit();
                }
                break;
            case "detalle":
                id = getIntent().getStringExtra(ProductoActivity.EXTRA_DETALLE_ID);
                int sec = getIntent().getIntExtra("SecuenciaDetalle", 0);
                DetallePedidoDetailFragment dpdfragment = DetallePedidoDetailFragment.newInstance(id, sec);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.producto_detail_container, dpdfragment)
                        .commit();
                break;
            case "cabecera":
                id = getIntent().getStringExtra(ProductoActivity.EXTRA_CABECERA_ID);
                CabeceraDetailFragment cabfragment = CabeceraDetailFragment.newInstance(id);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.producto_detail_container, cabfragment)
                        .commit();
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_producto_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}