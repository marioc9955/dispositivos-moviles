package uce.edu.ec.fing.pedidos.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.DetallePedidoFragment;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetallePedidoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetallePedidoDetailFragment extends Fragment {

    private static final String ARG_DETALLE_ID = "detalleId";
    private static final String ARG_DETALLE_SEC = "detalleSecuencia";

    private String mDetalleId;
    private int mDetSecuencia;

    private CollapsingToolbarLayout mCollapsingView;
    private TextView mCantidad;
    private TextView mProducto;
    private TextView mPrecio;

    private OperacionesBaseDatos mDbHelper;

    public DetallePedidoDetailFragment() {
        // Required empty public constructor
    }

    public static DetallePedidoDetailFragment newInstance(String detalleId, int secuencia) {
        DetallePedidoDetailFragment fragment = new DetallePedidoDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DETALLE_ID, detalleId);
        args.putInt(ARG_DETALLE_SEC, secuencia);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDetalleId = getArguments().getString(ARG_DETALLE_ID);
            mDetSecuencia = getArguments().getInt(ARG_DETALLE_SEC);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detalle_pedido_detail, container, false);
        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        mCantidad = (TextView) root.findViewById(R.id.tv_cant);
        mProducto = (TextView) root.findViewById(R.id.tv_producto);
        mPrecio = (TextView) root.findViewById(R.id.tv_precio);

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        loadDetalles();
        return root;
    }

    private void loadDetalles() {
        new GetDetalleByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                new DeleteDetalleTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DetallePedidoFragment.REQUEST_UPDATE_DELETE_DETALLE) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showDetalle(DetallePedido detallePedido) {
        mCollapsingView.setTitle(Integer.toString(detallePedido.secuencia));
        mCantidad.setText(String.valueOf(detallePedido.cantidad));
        mProducto.setText(mDbHelper.obtenerProductoID(detallePedido.idProducto).nombre);
        mPrecio.setText(String.valueOf(detallePedido.precio));
    }

    private void showEditScreen() {
        ProductoActivity.tablaActual = "detalle";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_DETALLE_ID, mDetalleId);
        intent.putExtra("SecuenciaDetalle", mDetSecuencia);
        startActivityForResult(intent, DetallePedidoFragment.REQUEST_UPDATE_DELETE_DETALLE);
    }

    private void showDetallesScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        ProductoActivity.tablaActual = "detalle";
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar detalle", Toast.LENGTH_SHORT).show();
    }

    private class GetDetalleByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerDetallesPorIdPedidoYSec(mDetalleId, mDetSecuencia);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showDetalle(new DetallePedido(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteDetalleTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mDbHelper.eliminarDetallePedido(mDetalleId, mDetSecuencia);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showDetallesScreen(integer > 0);
        }

    }
}