package uce.edu.ec.fing.pedidos.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.ProductoCursorAdapter;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.detail.DetailActivity;

public class ProductoFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_PRODUCTO = 2;

    private ListView mProductosList;
    private ProductoCursorAdapter mProductosAdapter;
    private FloatingActionButton mAddButton;

    private OperacionesBaseDatos mDbHelper;

    public static ProductoFragment newInstance(){
        return new ProductoFragment();
    }

    public ProductoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        View root = inflater.inflate(R.layout.fragment_producto, container, false);
        // Referencias UI
        mProductosList = (ListView) root.findViewById(R.id.productos_list);
        mProductosAdapter = new ProductoCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mProductosList.setAdapter(mProductosAdapter);

        // Eventos
        mProductosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mProductosAdapter.getItem(i);
                String currentLawyerId = currentItem.getString(
                        currentItem.getColumnIndex(ContratoPedidos.Productos.ID));

                showDetailScreen(currentLawyerId);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });

        // Instancia de helper
        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        loadProductos();
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditActivity.REQUEST_ADD:
                    showSuccessfullSavedMessage();
                    loadProductos();
                    break;
                case REQUEST_UPDATE_DELETE_PRODUCTO:
                    loadProductos();
                    break;
            }
        }
    }

    private void loadProductos() {
        new ProductoLoadTask().execute();
    }

    private void showSuccessfullSavedMessage() {
        Toast.makeText(getActivity(),
                "Producto guardado correctamente", Toast.LENGTH_SHORT).show();
    }

    private void showAddScreen() {
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD);
    }

    private void showDetailScreen(String lawyerId) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_PRODUCTO_ID, lawyerId);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_PRODUCTO);
    }

    private class ProductoLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerProductos();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mProductosAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }

}