package uce.edu.ec.fing.pedidos.modelo;

import android.database.Cursor;

import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;

public class FormaPago {

    public String idFormaPago;

    public String nombre;
//constructor que se llama cuando se va a actualizar y se requiere el id
    public FormaPago(String idFormaPago, String nombre) {
        this.idFormaPago = idFormaPago;
        this.nombre = nombre;
    }
    //constructor que se llama cuando se va a insertar datos y no se requiere id
    public FormaPago(String nombre) {

        this.nombre = nombre;
    }

    public FormaPago(Cursor cursor){
        idFormaPago = cursor.getString(cursor.getColumnIndex(ContratoPedidos.FormasPago.ID));
        nombre = cursor.getString(cursor.getColumnIndex(ContratoPedidos.FormasPago.NOMBRE));
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
