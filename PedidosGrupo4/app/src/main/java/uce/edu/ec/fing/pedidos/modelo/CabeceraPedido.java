package uce.edu.ec.fing.pedidos.modelo;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;

public class CabeceraPedido {

    public String idCabeceraPedido;

    public String fecha;

    public String idCliente;

    public String idFormaPago;

    public CabeceraPedido(String idCabeceraPedido, String fecha,
                          String idCliente, String idFormaPago) {
        this.idCabeceraPedido = idCabeceraPedido;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.idFormaPago = idFormaPago;
    }

    public CabeceraPedido(String fecha,
                          String idCliente, String idFormaPago) {
        this.idCabeceraPedido = ContratoPedidos.CabecerasPedido.generarIdCabeceraPedido();
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.idFormaPago = idFormaPago;
    }

    public CabeceraPedido(Cursor cursor){
        idCabeceraPedido = cursor.getString(cursor.getColumnIndex(ContratoPedidos.CabecerasPedido.ID));
        fecha = cursor.getString(cursor.getColumnIndex(ContratoPedidos.CabecerasPedido.FECHA));
        idCliente = cursor.getString(cursor.getColumnIndex(ContratoPedidos.CabecerasPedido.ID_CLIENTE));
        idFormaPago = cursor.getString(cursor.getColumnIndex(ContratoPedidos.CabecerasPedido.ID_FORMA_PAGO));
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ContratoPedidos.CabecerasPedido.ID, idCabeceraPedido);
        values.put(ContratoPedidos.CabecerasPedido.FECHA, fecha);
        values.put(ContratoPedidos.CabecerasPedido.ID_CLIENTE, idCliente);
        values.put(ContratoPedidos.CabecerasPedido.ID_FORMA_PAGO, idFormaPago);
        return values;
    }

    @NonNull
    @Override
    public String toString() {
        return fecha;
    }
}
