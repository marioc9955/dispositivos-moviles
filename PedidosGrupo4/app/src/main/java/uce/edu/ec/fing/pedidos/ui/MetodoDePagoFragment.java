package uce.edu.ec.fing.pedidos.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.MetodoDePagoCursorAdapter;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.detail.DetailActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MetodoDePagoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MetodoDePagoFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_MDP = 2;

    private ListView mMDPList;
    private MetodoDePagoCursorAdapter mMDPAdapter;
    private FloatingActionButton mAddButton;

    private OperacionesBaseDatos mDbHelper;

    public MetodoDePagoFragment() {
        // Required empty public constructor
    }

    public static MetodoDePagoFragment newInstance() {
        MetodoDePagoFragment fragment = new MetodoDePagoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_metodo_de_pago, container, false);
        // Referencias UI
        mMDPList = (ListView) root.findViewById(R.id.mdp_list);
        mMDPAdapter = new MetodoDePagoCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mMDPList.setAdapter(mMDPAdapter);

        // Eventos
        mMDPList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mMDPAdapter.getItem(i);
                String currentLawyerId = currentItem.getString(
                        currentItem.getColumnIndex(ContratoPedidos.Productos.ID));

                showDetailScreen(currentLawyerId);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });

        //getActivity().deleteDatabase(BaseDatosPedidos.NOMBRE_BASE_DATOS);

        // Instancia de helper
        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        loadProductos();
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditActivity.REQUEST_ADD:
                    showSuccessfullSavedMessage();
                    loadProductos();
                    break;
                case REQUEST_UPDATE_DELETE_MDP:
                    loadProductos();
                    break;
            }
        }
    }

    private void loadProductos() {
        new MDPLoadTask().execute();
    }

    private void showSuccessfullSavedMessage() {
        Toast.makeText(getActivity(),
                "Forma de pago guardada correctamente", Toast.LENGTH_SHORT).show();
    }

    private void showAddScreen() {
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD);
    }

    private void showDetailScreen(String Id) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_MDP_ID, Id);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_MDP);
    }

    private class MDPLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerFormasPago();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mMDPAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }
}