package uce.edu.ec.fing.pedidos.modelo;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;

public class DetalleCursorAdapter extends CursorAdapter {

    public DetalleCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Referencias UI.
        TextView nameText = (TextView) view.findViewById(R.id.tv_name);

        // Get valores.
        String name = cursor.getString(cursor.getColumnIndex(ContratoPedidos.DetallesPedido.SECUENCIA));

        // Setup.
        nameText.setText(name);
    }
}
