package uce.edu.ec.fing.pedidos.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;
import uce.edu.ec.fing.pedidos.ui.ProductoFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductoDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductoDetailFragment extends Fragment {

    private static final String ARG_PRODUCTO_ID = "productoId";

    private String mProductoId;

    private CollapsingToolbarLayout mCollapsingView;
    private TextView mPrecio;
    private TextView mExistencias;

    private OperacionesBaseDatos mDbHelper;

    public ProductoDetailFragment() {
        // Required empty public constructor
    }

    public static ProductoDetailFragment newInstance(String productoId) {
        ProductoDetailFragment fragment = new ProductoDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCTO_ID, productoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductoId = getArguments().getString(ARG_PRODUCTO_ID);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_producto_detail, container, false);

        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        mPrecio = (TextView) root.findViewById(R.id.tv_precio);
        mExistencias = (TextView) root.findViewById(R.id.tv_existencias);

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        loadProducto();

        return root;
    }
    private void loadProducto() {
        new GetProductoByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                new DeleteProductoTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProductoFragment.REQUEST_UPDATE_DELETE_PRODUCTO) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showProducto(Producto producto) {
        mCollapsingView.setTitle(producto.getNombre());
        mPrecio.setText(Float.toString(producto.getPrecio()));
        mExistencias.setText(Integer.toString(producto.getExistencias()) );
    }

    private void showEditScreen() {
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_PRODUCTO_ID, mProductoId);
        startActivityForResult(intent, ProductoFragment.REQUEST_UPDATE_DELETE_PRODUCTO);
    }

    private void showProductoScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar producto", Toast.LENGTH_SHORT).show();
    }
    private class GetProductoByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getProductoById(mProductoId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showProducto(new Producto(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteProductoTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mDbHelper.eliminarProducto(mProductoId);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showProductoScreen(integer > 0);
        }

    }
}