package uce.edu.ec.fing.pedidos.ui.addedit;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

public class AddEditClienteFragment extends Fragment {

    private static final String ARG_CLIENTE_ID = "arg_cliente_id";

    private String mClienteId;

    private OperacionesBaseDatos mDbHelper;

    private FloatingActionButton mSaveButton;
    private TextInputEditText mNombresField;
    private TextInputEditText mApellidosField;
    private TextInputEditText mTelefonoField;
    private TextInputLayout mNombresLabel;
    private TextInputLayout mApellidosLabel;
    private TextInputLayout mTelefonoLabel;

    public AddEditClienteFragment() {
        // Required empty public constructor
    }

    public static AddEditClienteFragment newInstance(String clienteId) {
        AddEditClienteFragment fragment = new AddEditClienteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CLIENTE_ID, clienteId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mClienteId = getArguments().getString(ARG_CLIENTE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_cliente, container, false);

        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        mNombresField = (TextInputEditText) root.findViewById(R.id.et_name);
        mApellidosField = (TextInputEditText) root.findViewById(R.id.et_apellido);
        mTelefonoField = (TextInputEditText) root.findViewById(R.id.et_phone);
        mNombresLabel = (TextInputLayout) root.findViewById(R.id.til_name);
        mApellidosLabel = (TextInputLayout) root.findViewById(R.id.til_apellido);
        mTelefonoLabel = (TextInputLayout) root.findViewById(R.id.til_phone);

        // Eventos
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEditCliente();
            }
        });

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        if (mClienteId != null) {
            loadCliente();
        }

        return root;
    }

    private void loadCliente() {
        new GetClienteByIdTask().execute();
    }

    private void addEditCliente() {
        ProductoActivity.tablaActual = "cliente";

        boolean error = false;

        String names = mNombresField.getText().toString();
        String apellido = mApellidosField.getText().toString();
        String telefono = mTelefonoField.getText().toString();

        if (TextUtils.isEmpty(names)) {
            mNombresLabel.setError("Ingresa un valor");
            error = true;
        }

        if (TextUtils.isEmpty(apellido)) {
            mApellidosLabel.setError("Ingresa un valor");
            error = true;
        }

        if (TextUtils.isEmpty(telefono)) {
            mTelefonoLabel.setError("Ingresa un valor");
            error = true;
        }

        if (error) {
            return;
        }

        Cliente cliente = new Cliente(names, apellido, telefono);

        new AddEditClienteTask().execute(cliente);

    }

    private void showClienteScreen(Boolean requery) {
        if (!requery) {
            showAddEditError();
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            getActivity().setResult(Activity.RESULT_OK);
        }

        getActivity().finish();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showCliente(Cliente cliente) {
        mNombresField.setText(cliente.nombres);
        mApellidosField.setText(cliente.apellidos);
        mTelefonoField.setText(cliente.telefono);
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar cliente", Toast.LENGTH_SHORT).show();
    }

    private class GetClienteByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getClienteById(mClienteId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showCliente(new Cliente(cursor));
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }

    private class AddEditClienteTask extends AsyncTask<Cliente, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Cliente... clientes) {
            if (mClienteId != null) {
                return mDbHelper.updateCliente(clientes[0], mClienteId) > 0;

            } else {
                return mDbHelper.insertCliente(clientes[0]) > 0;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            showClienteScreen(result);
        }

    }
}