package uce.edu.ec.fing.pedidos.ui.addedit;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.modelo.FormaPago;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEditCabeceraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEditCabeceraFragment extends Fragment {

    private static final String ARG_CABECERA_ID = "arg_cabecera_id";

    private String mCabeceraId;

    private OperacionesBaseDatos mDbHelper;

    private FloatingActionButton mSaveButton;
    private TextInputEditText mFechaField;
    private TextInputLayout mFechaLabel;

    Spinner spinnerClientes;
    Spinner spinnerMDP;

    public AddEditCabeceraFragment() {
        // Required empty public constructor
    }

    public static AddEditCabeceraFragment newInstance(String cabeceraId) {
        AddEditCabeceraFragment fragment = new AddEditCabeceraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CABECERA_ID, cabeceraId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCabeceraId = getArguments().getString(ARG_CABECERA_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_cabecera, container, false);;

        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        mFechaField = (TextInputEditText) root.findViewById(R.id.et_fecha);
        mFechaLabel = (TextInputLayout) root.findViewById(R.id.til_fecha);
        spinnerClientes = root.findViewById(R.id.spinner_clientes);
        spinnerMDP = root.findViewById(R.id.spinner_mdp);

        // Eventos
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEditCabecera();
            }
        });

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        if (mCabeceraId != null) {
            loadCabecera();
        }

        //carga de spiners
        List<Cliente> clientesList = mDbHelper.obtenerListaClientes();
        ArrayAdapter<Cliente> adapterC = new ArrayAdapter<Cliente>(getActivity(), android.R.layout.simple_spinner_dropdown_item, clientesList);
        spinnerClientes.setAdapter(adapterC);

        List<FormaPago> mdpList = mDbHelper.obtenerListaFormasPago();
        ArrayAdapter<FormaPago> adapterFP = new ArrayAdapter<FormaPago>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mdpList);
        spinnerMDP.setAdapter(adapterFP);

        return root;
    }


    private void loadCabecera() {
        new GetCabeceraByIdTask().execute();
    }

    private void addEditCabecera() {
        boolean error = false;

        String fecha = mFechaField.getText().toString();

        if (TextUtils.isEmpty(fecha)) {
            mFechaLabel.setError("Ingresa un valor");
            error = true;
        }

        if (error) {
            return;
        }

        String idCliente, idFormaPago;

        //obtener ids de spinners
        Cliente clienteSelec = (Cliente)spinnerClientes.getSelectedItem();
        idCliente = clienteSelec.idCliente;

        idFormaPago = ((FormaPago)spinnerMDP.getSelectedItem()).idFormaPago;

        CabeceraPedido cp = new CabeceraPedido(fecha, idCliente, idFormaPago);

        new AddEditCabeceraTask().execute(cp);

    }

    private void showCabeceraScreen(Boolean requery) {
        if (!requery) {
            showAddEditError();
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            getActivity().setResult(Activity.RESULT_OK);
        }

        getActivity().finish();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showCabecera(CabeceraPedido cp) {
        mFechaField.setText(cp.fecha);

        //setear spiners con datos de cabecera
        int pos = 0;
        for (int i = 0; i < spinnerClientes.getCount(); i++){
            String idCliente = ((Cliente)spinnerClientes.getItemAtPosition(i)).idCliente;
            if (idCliente.equalsIgnoreCase(cp.idCliente)){
                pos = i;
            }
        }
        spinnerClientes.setSelection(pos);

        for (int i = 0; i < spinnerMDP.getCount(); i++){
            String idFormaPago = ((FormaPago)spinnerMDP.getItemAtPosition(i)).idFormaPago;
            if (idFormaPago.equalsIgnoreCase(cp.idFormaPago)){
                pos = i;
            }
        }
        spinnerMDP.setSelection(pos);

    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar cabecera de pedido", Toast.LENGTH_SHORT).show();
    }

    private class GetCabeceraByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getCabeceraById(mCabeceraId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showCabecera(new CabeceraPedido(cursor));
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }

    private class AddEditCabeceraTask extends AsyncTask<CabeceraPedido, Void, Boolean> {

        @Override
        protected Boolean doInBackground(CabeceraPedido... ccabeceraPedidoss) {
            if (mCabeceraId != null) {
                return mDbHelper.updateCabecera(ccabeceraPedidoss[0], mCabeceraId) > 0;

            } else {
                return mDbHelper.insertCabecera(ccabeceraPedidoss[0]) > 0;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            showCabeceraScreen(result);
        }

    }
}