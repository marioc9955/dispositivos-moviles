package uce.edu.ec.fing.pedidos.ui.addedit;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEditProductoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEditProductoFragment extends Fragment {

    private static final String ARG_PRODUCTO_ID = "arg_producto_id";

    private String mProductoId;

    private OperacionesBaseDatos mDbHelper;

    private FloatingActionButton mSaveButton;
    private TextInputEditText mNombreField;
    private TextInputEditText mPrecioField;
    private TextInputEditText mExistenciasField;
    private TextInputLayout mNombreLabel;
    private TextInputLayout mPrecioLabel;
    private TextInputLayout mExistenciasLabel;


    public AddEditProductoFragment() {
        // Required empty public constructor
    }

    public static AddEditProductoFragment newInstance(String productoId) {
        AddEditProductoFragment fragment = new AddEditProductoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCTO_ID, productoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductoId = getArguments().getString(ARG_PRODUCTO_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_producto, container, false);

        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        mNombreField = (TextInputEditText) root.findViewById(R.id.et_name);
        mPrecioField = (TextInputEditText) root.findViewById(R.id.et_price);
        mExistenciasField = (TextInputEditText) root.findViewById(R.id.et_exist);
        mNombreLabel = (TextInputLayout) root.findViewById(R.id.til_name);
        mPrecioLabel = (TextInputLayout) root.findViewById(R.id.til_price);
        mExistenciasLabel = (TextInputLayout) root.findViewById(R.id.til_exist);

        // Eventos
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEditProducto();
            }
        });

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        if (mProductoId != null) {
            loadProducto();
        }

        return root;
    }

    private void loadProducto() {
        new GetProductoByIdTask().execute();
    }

    private void addEditProducto() {
        boolean error = false;

        String name = mNombreField.getText().toString();
        String precio = mPrecioField.getText().toString();
        String existencias = mExistenciasField.getText().toString();

        if (TextUtils.isEmpty(name)) {
            mNombreLabel.setError("Ingresa un valor");
            error = true;
        }

        if (TextUtils.isEmpty(precio)) {
            mPrecioLabel.setError("Ingresa un valor");
            error = true;
        }

        if (TextUtils.isEmpty(existencias)) {
            mExistenciasLabel.setError("Ingresa un valor");
            error = true;
        }

        if (error) {
            return;
        }

        Producto producto = new Producto(name, Float.parseFloat(precio), Integer.parseInt(existencias));

        new AddEditProductoTask().execute(producto);

    }

    private void showProductoScreen(Boolean requery) {
        if (!requery) {
            showAddEditError();
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            getActivity().setResult(Activity.RESULT_OK);
        }

        getActivity().finish();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showProducto(Producto producto) {
        mNombreField.setText(producto.getNombre());
        mPrecioField.setText(producto.getPrecio()+"");
        mExistenciasField.setText(producto.getExistencias()+"");
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar producto", Toast.LENGTH_SHORT).show();
    }

    private class GetProductoByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getProductoById(mProductoId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showProducto(new Producto(cursor));
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }

    private class AddEditProductoTask extends AsyncTask<Producto, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Producto... productos) {
            if (mProductoId != null) {
                return mDbHelper.updateProducto(productos[0], mProductoId) > 0;

            } else {
                return mDbHelper.insertProducto(productos[0]) > 0;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            showProductoScreen(result);
        }

    }
}