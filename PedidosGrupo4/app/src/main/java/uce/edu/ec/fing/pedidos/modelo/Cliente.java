package uce.edu.ec.fing.pedidos.modelo;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;

public class Cliente {

    public String idCliente;

    public String nombres;

    public String apellidos;

    public String telefono;

    public Cliente(String idCliente, String nombres, String apellidos, String telefono) {
        this.idCliente = idCliente;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
    }

    public Cliente(String nombres, String apellidos, String telefono) {
        this.idCliente = ContratoPedidos.Clientes.generarIdCliente();
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
    }

    public Cliente(Cursor cursor){
        idCliente = cursor.getString(cursor.getColumnIndex(ContratoPedidos.Clientes.ID));
        nombres = cursor.getString(cursor.getColumnIndex(ContratoPedidos.Clientes.NOMBRES));
        apellidos = cursor.getString(cursor.getColumnIndex(ContratoPedidos.Clientes.APELLIDOS));
        telefono = cursor.getString(cursor.getColumnIndex(ContratoPedidos.Clientes.TELEFONO));
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ContratoPedidos.Clientes.ID, idCliente);
        values.put(ContratoPedidos.Clientes.NOMBRES, nombres);
        values.put(ContratoPedidos.Clientes.APELLIDOS, apellidos);
        values.put(ContratoPedidos.Clientes.TELEFONO, telefono);
        return values;
    }

    @Override
    public String toString(){
        return nombres + " " + apellidos;
    }
}
