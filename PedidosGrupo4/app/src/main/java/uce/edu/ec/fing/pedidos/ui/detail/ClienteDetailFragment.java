package uce.edu.ec.fing.pedidos.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.ClienteFragment;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

public class ClienteDetailFragment extends Fragment {

    private static final String ARG_CLIENTE_ID = "clienteId";

    private String mClienteId;

    private CollapsingToolbarLayout mCollapsingView;
    private TextView mApellidos;
    private TextView mTelefono;

    private OperacionesBaseDatos mDbHelper;

    public ClienteDetailFragment() {
        // Required empty public constructor
    }

    public static ClienteDetailFragment newInstance(String clienteId) {
        ClienteDetailFragment fragment = new ClienteDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CLIENTE_ID, clienteId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mClienteId = getArguments().getString(ARG_CLIENTE_ID);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cliente_detail, container, false);

        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        mApellidos = (TextView) root.findViewById(R.id.tv_apellidos);
        mTelefono = (TextView) root.findViewById(R.id.tv_telefono);

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        loadCliente();
        return root;
    }

    private void loadCliente() {
        new GetClienteByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                new DeleteClienteTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ClienteFragment.REQUEST_UPDATE_DELETE_CLIENTE) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showCliente(Cliente cliente) {
        mCollapsingView.setTitle(cliente.nombres);
        mApellidos.setText(cliente.apellidos);
        mTelefono.setText(cliente.telefono);
    }

    private void showEditScreen() {
        ProductoActivity.tablaActual = "cliente";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_CLIENTE_ID, mClienteId);
        startActivityForResult(intent, ClienteFragment.REQUEST_UPDATE_DELETE_CLIENTE);
    }

    private void showClienteScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        ProductoActivity.tablaActual = "cliente";
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar cliente", Toast.LENGTH_SHORT).show();
    }
    private class GetClienteByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getClienteById(mClienteId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showCliente(new Cliente(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteClienteTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mDbHelper.eliminarCliente(mClienteId);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showClienteScreen(integer > 0);
        }

    }
}