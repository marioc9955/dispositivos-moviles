package uce.edu.ec.fing.pedidos.ui.addedit;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;

public class AddEditActivity extends AppCompatActivity {

    public static final int REQUEST_ADD = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String tablaId;

        switch (ProductoActivity.tablaActual){
            case "producto":
                tablaId = getIntent().getStringExtra(ProductoActivity.EXTRA_PRODUCTO_ID);
                setTitle(tablaId == null ? "Añadir producto" : "Editar producto");

                AddEditProductoFragment addEditProductoFragment = (AddEditProductoFragment)
                        getSupportFragmentManager().findFragmentById(R.id.add_edit_producto_container);
                if (addEditProductoFragment == null) {
                    addEditProductoFragment = AddEditProductoFragment.newInstance(tablaId);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.add_edit_producto_container, addEditProductoFragment)
                            .commit();
                }
                break;
            case "cliente":
                tablaId = getIntent().getStringExtra(ProductoActivity.EXTRA_CLIENTE_ID);
                setTitle(tablaId == null ? "Añadir cliente" : "Editar cliente");

                AddEditClienteFragment addEditClienteFragment = (AddEditClienteFragment)
                        getSupportFragmentManager().findFragmentById(R.id.add_edit_producto_container);
                if (addEditClienteFragment == null) {
                    addEditClienteFragment = AddEditClienteFragment.newInstance(tablaId);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.add_edit_producto_container, addEditClienteFragment)
                            .commit();
                }
                break;
            case "mdp":
                tablaId = getIntent().getStringExtra(ProductoActivity.EXTRA_MDP_ID);
                setTitle(tablaId == null ? "Añadir forma de pago" : "Editar forma de pago");

                AddEditMetodoPagoFragment addEditMetodoPagoFragment = (AddEditMetodoPagoFragment)
                        getSupportFragmentManager().findFragmentById(R.id.add_edit_producto_container);
                if (addEditMetodoPagoFragment == null) {
                    addEditMetodoPagoFragment = AddEditMetodoPagoFragment.newInstance(tablaId);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.add_edit_producto_container, addEditMetodoPagoFragment)
                            .commit();
                }
                break;
            case "cabecera":
                tablaId = getIntent().getStringExtra(ProductoActivity.EXTRA_CABECERA_ID);
                setTitle(tablaId == null ? "Añadir Pedido" : "Editar Pedido");

                AddEditCabeceraFragment addEditCabeceraFragment = (AddEditCabeceraFragment)
                        getSupportFragmentManager().findFragmentById(R.id.add_edit_producto_container);
                if (addEditCabeceraFragment == null) {
                    addEditCabeceraFragment = AddEditCabeceraFragment.newInstance(tablaId);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.add_edit_producto_container, addEditCabeceraFragment)
                            .commit();
                }
                break;
            case "detalle":
                tablaId = getIntent().getStringExtra(ProductoActivity.EXTRA_DETALLE_ID);
                int sec = getIntent().getIntExtra("SecuenciaDetalle", 0);
                setTitle(tablaId == null ? "Añadir Detalle Pedido" : "Editar Detalle Pedido");

                AddEditDetalleFragment addEditDetalleFragment = (AddEditDetalleFragment)
                        getSupportFragmentManager().findFragmentById(R.id.add_edit_producto_container);
                if (addEditDetalleFragment == null) {
                    addEditDetalleFragment = AddEditDetalleFragment.newInstance(tablaId, sec);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.add_edit_producto_container, addEditDetalleFragment)
                            .commit();
                }
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}