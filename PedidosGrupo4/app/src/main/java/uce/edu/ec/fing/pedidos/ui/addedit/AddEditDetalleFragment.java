package uce.edu.ec.fing.pedidos.ui.addedit;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Comparator;
import java.util.List;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEditDetalleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEditDetalleFragment extends Fragment {

    private static final String ARG_DETALLE_ID = "arg_detalle_id";

    private String mDetalleId;
    private int mDetalleSec;

    private OperacionesBaseDatos mDbHelper;

    private FloatingActionButton mSaveButton;
    private TextInputEditText mCantidadField;
    private TextInputLayout mCantidadLabel;

    Spinner spinnerCabecera;
    Spinner spinnerProducto;

    public AddEditDetalleFragment() {
        // Required empty public constructor
    }

    public static AddEditDetalleFragment newInstance(String detalleId, int detalleSec) {
        AddEditDetalleFragment fragment = new AddEditDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DETALLE_ID, detalleId);
        args.putInt("SecuenciaDetalle", detalleSec);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDetalleId = getArguments().getString(ARG_DETALLE_ID);
            mDetalleSec = getArguments().getInt("SecuenciaDetalle");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_detalle, container, false);

        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        mCantidadField = (TextInputEditText) root.findViewById(R.id.et_cantidad);
        mCantidadLabel = (TextInputLayout) root.findViewById(R.id.til_cantidad);
        spinnerCabecera = root.findViewById(R.id.spinner_cabecera);
        spinnerProducto = root.findViewById(R.id.spinner_producto);

        // Eventos
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEditDetalle();
            }
        });

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        if (mDetalleId != null) {
            loadDetalle();
        }

        //carga de spiners
        List<CabeceraPedido> cabecerasList = mDbHelper.obtenerListaCabeceras();
        ArrayAdapter<CabeceraPedido> adapterCabecera = new ArrayAdapter<CabeceraPedido>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, cabecerasList);
        spinnerCabecera.setAdapter(adapterCabecera);

        List<Producto> productosList = mDbHelper.obtenerListaProductos();
        ArrayAdapter<Producto> adapterProducto = new ArrayAdapter<Producto>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, productosList);
        spinnerProducto.setAdapter(adapterProducto);

        return root;
    }

    private void loadDetalle() {
        new GetDetalleByIdTask().execute();
    }

    private void addEditDetalle() {
        boolean error = false;

        String cantidad = mCantidadField.getText().toString();

        if (TextUtils.isEmpty(cantidad)) {
            mCantidadLabel.setError("Ingresa un valor");
            error = true;
        }

        if (error) {
            return;
        }

        String idCabecera, idProducto;

        //obtener ids de spinners
        CabeceraPedido cabeceraSelect = (CabeceraPedido) spinnerCabecera.getSelectedItem();
        idCabecera = cabeceraSelect.idCabeceraPedido;

        idProducto = ((Producto)spinnerProducto.getSelectedItem()).idProducto;

        int secuencia;
        List<DetallePedido> detalles = mDbHelper.obtenerListaDetallesPorIdCabecera(idCabecera);
        detalles.sort(new Comparator<DetallePedido>() {
            @Override
            public int compare(DetallePedido o1, DetallePedido o2) {
                return -Integer.compare(o1.secuencia, o2.secuencia);
            }
        });

        if (mDetalleId != null){
            secuencia = mDetalleSec;
        }else{
            if (detalles.size()>0){
                secuencia = detalles.get(0).secuencia + 1;
            }else{
                secuencia = 1;
            }

        }

        System.out.println(secuencia+" secuencia del addeditdetalle");

        DetallePedido dp = new DetallePedido(idCabecera, secuencia, idProducto,
                Integer.parseInt(cantidad), mDbHelper.obtenerProductoID(idProducto).precio);

        new AddEditDetalleTask().execute(dp);

    }

    private void showDetalleScreen(Boolean requery) {
        if (!requery) {
            showAddEditError();
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            getActivity().setResult(Activity.RESULT_OK);
        }

        getActivity().finish();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showDetalle(DetallePedido dp) {
        mCantidadField.setText(String.valueOf(dp.cantidad));

        //setear spiners con datos de cabecera
        int pos = 0;
        for (int i = 0; i < spinnerCabecera.getCount(); i++){
            String idCabecera = ((CabeceraPedido) spinnerCabecera.getItemAtPosition(i)).idCabeceraPedido;
            if (idCabecera.equalsIgnoreCase(dp.idCabeceraPedido)){
                pos = i;
            }
        }
        spinnerCabecera.setSelection(pos);

        for (int i = 0; i < spinnerProducto.getCount(); i++){
            String idProducto = ((Producto) spinnerProducto.getItemAtPosition(i)).idProducto;
            if (idProducto.equalsIgnoreCase(dp.idProducto)){
                pos = i;
            }
        }
        spinnerProducto.setSelection(pos);
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar detalle de pedido", Toast.LENGTH_SHORT).show();
    }

    private class GetDetalleByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerDetallesPorIdPedidoYSec(mDetalleId, mDetalleSec);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showDetalle(new DetallePedido(cursor));
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }

    private class AddEditDetalleTask extends AsyncTask<DetallePedido, Void, Boolean> {

        @Override
        protected Boolean doInBackground(DetallePedido... detalles) {
            if (mDetalleId != null) {
                return mDbHelper.updateDetalle(detalles[0], mDetalleId, mDetalleSec) > 0;

            } else {
                return mDbHelper.insertDetallePedido(detalles[0]) > 0;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            showDetalleScreen(result);
        }

    }
}