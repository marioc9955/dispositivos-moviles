package uce.edu.ec.fing.pedidos.ui.addedit;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.FormaPago;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEditMetodoPagoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEditMetodoPagoFragment extends Fragment {

    private static final String ARG_MDP_ID = "arg_mdp_id";

    private String mMDPId;

    private OperacionesBaseDatos mDbHelper;

    private FloatingActionButton mSaveButton;
    private TextInputEditText mNombreField;
    private TextInputLayout mNombreLabel;

    public AddEditMetodoPagoFragment() {
        // Required empty public constructor
    }

    public static AddEditMetodoPagoFragment newInstance(String mdpId) {
        AddEditMetodoPagoFragment fragment = new AddEditMetodoPagoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MDP_ID, mdpId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMDPId = getArguments().getString(ARG_MDP_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit_metodo_pago, container, false);
        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        mNombreField = (TextInputEditText) root.findViewById(R.id.et_name);
        mNombreLabel = (TextInputLayout) root.findViewById(R.id.til_name);

        // Eventos
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEditFormaPago();
            }
        });

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        if (mMDPId != null) {
            loadFormaPago();
        }
        return root;
    }

    private void loadFormaPago() {
        new GetMDPByIdTask().execute();
    }

    private void addEditFormaPago() {
        boolean error = false;

        String name = mNombreField.getText().toString();

        if (TextUtils.isEmpty(name)) {
            mNombreLabel.setError("Ingresa un valor");
            error = true;
        }

        if (error) {
            return;
        }

        FormaPago mdp = new FormaPago(name);

        new AddEditMDPTask().execute(mdp);

    }

    private void showMDPScreen(Boolean requery) {
        if (!requery) {
            showAddEditError();
            getActivity().setResult(Activity.RESULT_CANCELED);
        } else {
            getActivity().setResult(Activity.RESULT_OK);
        }

        getActivity().finish();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showMDP(FormaPago mdp) {
        mNombreField.setText(mdp.nombre);
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar forma de pago", Toast.LENGTH_SHORT).show();
    }

    private class GetMDPByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.getMDPById(mMDPId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showMDP(new FormaPago(cursor));
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }

    private class AddEditMDPTask extends AsyncTask<FormaPago, Void, Boolean> {

        @Override
        protected Boolean doInBackground(FormaPago... mdps) {
            if (mMDPId != null) {
                return mDbHelper.actualizarFormaPago(new FormaPago(mMDPId, mdps[0].nombre)) > 0;

            } else {
                return mDbHelper.insertFormaPago(mdps[0]) > 0;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            showMDPScreen(result);
        }

    }
}