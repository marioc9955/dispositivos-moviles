package uce.edu.ec.fing.pedidos.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.CabeceraCursorAdapter;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.detail.DetailActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CabeceraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CabeceraFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_CABECERA = 2;

    private ListView mCabeceraList;
    private CabeceraCursorAdapter mCabeceraAdapter;
    private FloatingActionButton mAddButton;

    private OperacionesBaseDatos mDbHelper;

    public CabeceraFragment() {
        // Required empty public constructor
    }

    public static CabeceraFragment newInstance() {
        CabeceraFragment fragment = new CabeceraFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cabecera, container, false);
        // Referencias UI
        mCabeceraList = (ListView) root.findViewById(R.id.cabecera_list);
        mCabeceraAdapter = new CabeceraCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mCabeceraList.setAdapter(mCabeceraAdapter);

        // Eventos
        mCabeceraList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mCabeceraAdapter.getItem(i);
                String currentClienteId = currentItem.getString(
                        currentItem.getColumnIndex(ContratoPedidos.CabecerasPedido.ID));

                showDetailScreen(currentClienteId);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });

        // Instancia de helper
        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        loadCabeceras();
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditActivity.REQUEST_ADD:
                    showSuccessfullSavedMessage();
                    loadCabeceras();
                    break;
                case REQUEST_UPDATE_DELETE_CABECERA:
                    loadCabeceras();
                    break;
            }
        }
    }

    private void loadCabeceras() {
        new CabeceraLoadTask().execute();
    }

    private void showSuccessfullSavedMessage() {
        Toast.makeText(getActivity(),
                "Cabecera guardada correctamente", Toast.LENGTH_SHORT).show();
    }

    private void showAddScreen() {
        ProductoActivity.tablaActual = "cabecera";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD);
    }

    private void showDetailScreen(String lawyerId) {
        ProductoActivity.tablaActual = "cabecera";
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_CABECERA_ID, lawyerId);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_CABECERA);
    }

    private class CabeceraLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerCabecerasPedidos();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mCabeceraAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }
}