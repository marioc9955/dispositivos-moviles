package uce.edu.ec.fing.pedidos.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.pedidos.modelo.Cliente;
import uce.edu.ec.fing.pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.pedidos.modelo.Producto;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.CabeceraFragment;
import uce.edu.ec.fing.pedidos.ui.ProductoActivity;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CabeceraDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CabeceraDetailFragment extends Fragment {

    private static final String ARG_CABECERA_ID = "cabeceraId";

    private String mCabeceraId;

    private CollapsingToolbarLayout mCollapsingView;
    private TextView mCliente;
    private TextView mMdp;
    private ListView mPedidosList;

    private OperacionesBaseDatos mDbHelper;
    public CabeceraDetailFragment() {
        // Required empty public constructor
    }

    public static CabeceraDetailFragment newInstance(String cabeceraId) {
        CabeceraDetailFragment fragment = new CabeceraDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CABECERA_ID, cabeceraId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCabeceraId = getArguments().getString(ARG_CABECERA_ID);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cabecera_detail, container, false);

        mCollapsingView = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        mCliente = (TextView) root.findViewById(R.id.tv_cliente);
        mMdp = root.findViewById(R.id.tv_mdp);
        mPedidosList = (ListView) root.findViewById(R.id.list_pedidos);

        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        loadCabecera();

        return root;
    }

    private void loadCabecera() {
        new GetCabeceraByIdTask().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                showEditScreen();
                break;
            case R.id.action_delete:
                new DeleteCabeceraTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CabeceraFragment.REQUEST_UPDATE_DELETE_CABECERA) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    private void showCabecera(CabeceraPedido cabeceraPedido) {
        mCollapsingView.setTitle(cabeceraPedido.fecha);
        Cliente c = mDbHelper.obtenerClientePorId(cabeceraPedido.idCliente);
        mCliente.setText(c.nombres +" "+c.apellidos);

        mMdp.setText(mDbHelper.obtenerFormaPagoID(cabeceraPedido.idFormaPago).nombre);

        ArrayList<String> pedidosString = new ArrayList<>();

        List<DetallePedido> pedidoList = mDbHelper.obtenerListaDetallesPorIdCabecera(cabeceraPedido.idCabeceraPedido);
        for (int i = 0; i < pedidoList.size(); i++){
            pedidosString.add("Pedido " + pedidoList.get(i).secuencia+": " + pedidoList.get(i).cantidad +" de "
                    + mDbHelper.obtenerProductoID(pedidoList.get(i).idProducto).nombre
            +" al precio unitario de " + pedidoList.get(i).precio);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, pedidosString);
        mPedidosList.setAdapter(adapter);
    }

    private void showEditScreen() {
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_CABECERA_ID, mCabeceraId);
        startActivityForResult(intent, CabeceraFragment.REQUEST_UPDATE_DELETE_CABECERA);
    }

    private void showCabeceraScreen(boolean requery) {
        if (!requery) {
            showDeleteError();
        }
        getActivity().setResult(requery ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    private void showDeleteError() {
        Toast.makeText(getActivity(),
                "Error al eliminar cabecera", Toast.LENGTH_SHORT).show();
    }
    private class GetCabeceraByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerCabeceraCursorPorId(mCabeceraId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showCabecera(new CabeceraPedido(cursor));
            } else {
                showLoadError();
            }
        }

    }

    private class DeleteCabeceraTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            return mDbHelper.eliminarCabeceraPedido(mCabeceraId);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            showCabeceraScreen(integer > 0);
        }

    }
}