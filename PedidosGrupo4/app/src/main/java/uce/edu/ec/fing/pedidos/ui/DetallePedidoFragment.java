package uce.edu.ec.fing.pedidos.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.pedidos.R;
import uce.edu.ec.fing.pedidos.modelo.DetalleCursorAdapter;
import uce.edu.ec.fing.pedidos.sqlite.ContratoPedidos;
import uce.edu.ec.fing.pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.pedidos.ui.addedit.AddEditActivity;
import uce.edu.ec.fing.pedidos.ui.detail.DetailActivity;

public class DetallePedidoFragment extends Fragment {

    public static final int REQUEST_UPDATE_DELETE_DETALLE = 2;

    private ListView mDetalleList;
    private DetalleCursorAdapter mDetalleAdapter;
    private FloatingActionButton mAddButton;

    private OperacionesBaseDatos mDbHelper;

    public DetallePedidoFragment() {
        // Required empty public constructor
    }

    public static DetallePedidoFragment newInstance() {
        DetallePedidoFragment fragment = new DetallePedidoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detalle_pedido, container, false);
        // Referencias UI
        mDetalleList = (ListView) root.findViewById(R.id.detalle_list);
        mDetalleAdapter = new DetalleCursorAdapter(getActivity(), null);
        mAddButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        // Setup
        mDetalleList.setAdapter(mDetalleAdapter);

        // Eventos
        mDetalleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mDetalleAdapter.getItem(i);
                String currentDetalleId = currentItem.getString(
                        currentItem.getColumnIndex(ContratoPedidos.DetallesPedido.ID));
                int currentDetalleSec = currentItem.getInt(
                        currentItem.getColumnIndex(ContratoPedidos.DetallesPedido.SECUENCIA));

                showDetailScreen(currentDetalleId, currentDetalleSec);
            }
        });
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddScreen();
            }
        });

        // Instancia de helper
        mDbHelper = OperacionesBaseDatos.obtenerInstancia(getActivity());

        // Carga de datos
        loadDetalles();
        return root;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case AddEditActivity.REQUEST_ADD:
                    showSuccessfullSavedMessage();
                    loadDetalles();
                    break;
                case REQUEST_UPDATE_DELETE_DETALLE:
                    loadDetalles();
                    break;
            }
        }
    }

    private void loadDetalles() {
        new DetalleLoadTask().execute();
    }

    private void showSuccessfullSavedMessage() {
        Toast.makeText(getActivity(),
                "Detalle de pedido guardado correctamente", Toast.LENGTH_SHORT).show();
    }

    private void showAddScreen() {
        ProductoActivity.tablaActual = "detalle";
        Intent intent = new Intent(getActivity(), AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD);
    }

    private void showDetailScreen(String lawyerId, int secuencia) {
        ProductoActivity.tablaActual = "detalle";
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(ProductoActivity.EXTRA_DETALLE_ID, lawyerId);
        intent.putExtra("SecuenciaDetalle", secuencia);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_DETALLE);
    }

    private class DetalleLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDbHelper.obtenerDetallesPedido();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mDetalleAdapter.swapCursor(cursor);
            } else {
                // Mostrar empty state
            }
        }
    }
}