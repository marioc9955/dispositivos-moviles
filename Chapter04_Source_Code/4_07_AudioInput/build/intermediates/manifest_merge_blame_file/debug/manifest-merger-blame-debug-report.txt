1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.audioin"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
7-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:7:5-42
8        android:minSdkVersion="3"
8-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:7:15-40
9        android:targetSdkVersion="24" />
9-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:7:5-42
10
11    <uses-permission android:name="android.permission.RECORD_AUDIO" />
11-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:9:5-87
11-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:9:22-68
12    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
12-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:10:5-97
12-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:10:22-78
13
14    <application
14-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:12:5-24:19
15        android:debuggable="true"
16        android:icon="@drawable/icon"
16-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:13:9-38
17        android:label="@string/app_name"
17-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:14:9-41
18        android:testOnly="true" >
19        <activity
19-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:15:9-22:20
20            android:name="com.androidrecipes.audioin.RecognizeActivity"
20-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:16:13-46
21            android:label="@string/app_name" >
21-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:17:13-45
22            <intent-filter>
22-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:18:13-21:29
23                <action android:name="android.intent.action.MAIN" />
23-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:19:17-68
23-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:19:25-66
24
25                <category android:name="android.intent.category.LAUNCHER" />
25-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:20:17-76
25-->C:\Users\mario\Escritorio\semestre 20-21\Dispositivos moviles\android-recipes-5ed-master\Chapter04_Source_Code\4_07_AudioInput\src\main\AndroidManifest.xml:20:27-74
26            </intent-filter>
27        </activity>
28    </application>
29
30</manifest>
