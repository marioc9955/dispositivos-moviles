package uce.edu.ec.fing.juego2d;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

//import static uce.edu.ec.fing.juego2d.GameView.screenRatioX;
//import static uce.edu.ec.fing.juego2d.GameView.screenRatioY;

public class Monstruo {
    public int speed = 20;
    public boolean wasShot = true;
    int x = 0, y, width, height, animCounter;
    Bitmap monstruo1, monstruo2, monstruo3, monstruo4, monstruo5, monstruo6;

    int numRepeticionesFrames = 3;

    Monstruo(Resources res) {

        monstruo1 = BitmapFactory.decodeResource(res, R.drawable.monstruo1);
        monstruo2 = BitmapFactory.decodeResource(res, R.drawable.monstruo2);
        monstruo3 = BitmapFactory.decodeResource(res, R.drawable.monstruo3);
        monstruo4 = BitmapFactory.decodeResource(res, R.drawable.monstruo4);
        monstruo5 = BitmapFactory.decodeResource(res, R.drawable.monstruo5);
        monstruo6 = BitmapFactory.decodeResource(res, R.drawable.monstruo6);

        width = monstruo1.getWidth();
        height = monstruo1.getHeight();

        width /= 4;
        height /= 4;

       // width = (int) (width * screenRatioX);
       // height = (int) (height * screenRatioY);

        monstruo1 = Bitmap.createScaledBitmap(monstruo1, width, height, false);
        monstruo2 = Bitmap.createScaledBitmap(monstruo2, width, height, false);
        monstruo3 = Bitmap.createScaledBitmap(monstruo3, width, height, false);
        monstruo4 = Bitmap.createScaledBitmap(monstruo4, width, height, false);
        monstruo5 = Bitmap.createScaledBitmap(monstruo5, width, height, false);
        monstruo6 = Bitmap.createScaledBitmap(monstruo6, width, height, false);

        y = -height;

        x = 0;

        animCounter = (int) (Math.random()*(6*numRepeticionesFrames)+numRepeticionesFrames);
    }

    Bitmap getMonstruoImage() {

        //if (animCounter == 1) {
        if (animCounter <= numRepeticionesFrames) {
            animCounter++;
            return monstruo1;
        }else
        if (animCounter <= 2*numRepeticionesFrames) {
            animCounter++;
            return monstruo2;
        }else
        if (animCounter <= 3*numRepeticionesFrames) {
            animCounter++;
            return monstruo3;
        }else
        if (animCounter <= 4*numRepeticionesFrames) {
            animCounter++;
            return monstruo4;
        }else
        if (animCounter <= 5*numRepeticionesFrames) {
            animCounter++;
            return monstruo5;
        }else
        if (animCounter > 6*numRepeticionesFrames) {
            animCounter = 1;
        }
        animCounter++;
        return monstruo6;

    }

    Rect getCollisionShape () {
        return new Rect(x, y,  x + width, y + height);
    }

    Collider getCollider(){
        return  new Collider(width * 0.4f, x + width*0.5f, y + height*0.5f);
    }

}
